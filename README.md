Il sistema permette al museo Mav di conoscere la valutazione dei visitatori al termine della visita. La valutazione sarà espressa tramite un questionario composto da una serie di domande relative alle quattro macro aree del museo.
Il questionario sarà anonimo e conservato per fini statistici e per migliorare il servizio fornito agli utenti futuri.    
Tramite il sistema, il Mav elaborerà una statistica dei dati raccolti dai questionari di gradimento.

Il sistema è gestito da un amministratore che previa autenticazione tramite login e password, può aggiungere o modificare le domande presenti nei questionari, inoltre può generare report sintetici e con grafici che possono essere visualizzati e stampati tramite interfaccia grafica.