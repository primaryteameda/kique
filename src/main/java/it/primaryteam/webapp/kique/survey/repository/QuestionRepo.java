package it.primaryteam.webapp.kique.survey.repository;

import java.util.Collection;
import java.util.List;

import it.primaryteam.webapp.kique.core.Repository;
import it.primaryteam.webapp.kique.email.Email;
import it.primaryteam.webapp.kique.survey.Survey;
import it.primaryteam.webapp.kique.survey.answer.Answer;
import it.primaryteam.webapp.kique.survey.choice.Choice;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.tips.Tip;

public interface QuestionRepo extends Repository<Long, Question> {

	List<Question> getAllBySurvey(Long idSurvey);

	List<Survey> getAllSurvey();

	List<Survey> getAllSurveyIt();

	List<Survey> getAllSurveyEn();

	void addSurvey(Survey entity);

	List<Survey> searchSurvey(String column, String value);

	void updateSurvey(Survey entity);

	void deleteSurvey(Survey survey);

	void addQuestion(Question question, Long idSurvey);
	
	void addQuestions(List<Question> questions, Long idSurvey);
 
	List<Question> getWithoutAnswer(Long idSurvey);

	void addAnswer(Answer answer, Long idQuestion);

	List<Answer> getAllAnswer();

	void addAnswers(List<Answer> answers, Long idQuestion);

	void deleteAnswer(Answer answer);

	void addEmail(Email email);
	
	void addTip(Tip tip);
	
	Collection<Tip> getAllTip();

	void addChoice(Choice choice);

	Collection<Email> getAllEmail();

	List<Question> getAllStatistic(Long idSurvey);
	
}
