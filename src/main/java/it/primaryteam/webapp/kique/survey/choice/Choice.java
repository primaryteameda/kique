package it.primaryteam.webapp.kique.survey.choice;

import it.primaryteam.webapp.kique.core.BaseDomain;


public class Choice extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;
	
	private Long idQuestion;
	private Long idAnswer;
	private int count;
	
	public Long getIdQuestion() {
		return idQuestion;
	}
	public void setIdQuestion(Long idQuestion) {
		this.idQuestion = idQuestion;
	}
	public Long getIdAnswer() {
		return idAnswer;
	}
	public void setIdAnswer(Long idAnswer) {
		this.idAnswer = idAnswer;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Choice [idQuestion=" + idQuestion + ", idAnswer=" + idAnswer + "]";
	}
}
