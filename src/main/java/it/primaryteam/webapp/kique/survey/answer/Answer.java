package it.primaryteam.webapp.kique.survey.answer;

import it.primaryteam.webapp.kique.core.BaseDomain;

public class Answer extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;

	private String anstext;
	private Long value;
	private String language;
	private int count;

	public Answer() {
		this.setAnstext("");
		this.setValue(0L);
		this.setLanguage("");
		this.setCount(0);
	}
	
	public String getAnstext() {
		return anstext;
	}
	public void setAnstext(String anstext) {
		this.anstext = anstext;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Answer [anstext=" + anstext + ", value=" + value + ", language=" + language + ", count=" + count + "]";
	}
	

}