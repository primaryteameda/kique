package it.primaryteam.webapp.kique.survey;

import java.util.ArrayList;
import java.util.List;
import it.primaryteam.webapp.kique.core.BaseDomain;
import it.primaryteam.webapp.kique.survey.question.Question;


public class Survey extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;

	private String typology;
	private String language;
	List<Question> questions = new ArrayList <Question>();

	public String getTypology() {
		return typology;
	}
	public void setTypology(String typology) {
		this.typology = typology;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	@Override
	public String toString() {
		return "Survey [typology=" + typology + ", language=" + language + ", questions=" + questions + "]";
	}

}
