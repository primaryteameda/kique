package it.primaryteam.webapp.kique.survey.repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.Dependent;

import it.primaryteam.webapp.kique.core.BaseRepository;
import it.primaryteam.webapp.kique.email.Email;
import it.primaryteam.webapp.kique.survey.Survey;
import it.primaryteam.webapp.kique.survey.answer.Answer;
import it.primaryteam.webapp.kique.survey.choice.Choice;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.tips.Tip;



@Dependent
public class Repository extends BaseRepository <Long, Question> implements QuestionRepo {

	public List<Question> getAllBySurvey(Long idSurvey) {
		String sql = String.format("SELECT q.id AS idQuestion, q.quetext AS question, "
				+ "a.id AS idAnswer, a.anstext AS answer, a.value, a.language FROM Questions q INNER JOIN Surveys s "
				+ "ON s.id = q.idsurvey INNER JOIN Answers a ON q.id = a.idquestion "
				+ "WHERE s.visibility = true AND q.visibility = true AND a.visibility = true AND s.id = %d ORDER BY idQuestion, a.value ", idSurvey);

		List<Map<String, Object>> results = select(sql);

		Map<Long, Question> questions = new LinkedHashMap<>();

		for(Map<String, Object> result : results) {

			Answer answer = new Answer();

			answer.setId((Long)result.get("idAnswer"));
			answer.setAnstext((String)result.get("answer"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);	

			if (!questions.containsKey((Long)result.get("idQuestion"))) {
				Question question = new Question();

				List<Answer> answers = new ArrayList<Answer>();
				answers.add(answer);

				question.setAnswers(answers);
				question.setId((Long)result.get("idQuestion"));
				question.setQuetext((String)result.get("question"));
				question.setLanguage((String)result.get("language"));
				question.setVisibility(true);

				questions.put((Long)result.get("idQuestion"), question);

			} else {
				Question question = questions.get((Long)result.get("idQuestion"));
				question.getAnswers().add(answer);
			}

		}

		return new ArrayList<Question>(questions.values()); 
	}
	
	@Override
	public List<Survey> getAllSurvey() {
	
	
		List<Survey> surveys = new ArrayList<Survey>();
	
		String sql = String.format("SELECT * FROM Surveys s WHERE s.visibility = true"); 
	
		List<Map<String, Object>> results = select(sql);
	
		for(Map<String, Object> result : results) {
	
			Question question = new Question();
			Answer answer = new Answer();
			Survey survey = new Survey();
	
			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);
	
			question.setId((Long)result.get("id"));
			question.setQuetext((String)result.get("quetext"));
			question.setLanguage((String)result.get("language"));
			question.setVisibility(true);
	
			answer.setId((Long)result.get("id"));
			answer.setAnstext((String)result.get("anstext"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);
	
			surveys.add(survey);
		}
	
		return surveys;
	
	}

	@Override
	public List<Survey> getAllSurveyIt() {
	
	
		List<Survey> surveys = new ArrayList<Survey>();
	
		String sql = String.format("SELECT * FROM Surveys s WHERE s.visibility = true AND s.language='IT'"); 
	
		List<Map<String, Object>> results = select(sql);
	
		for(Map<String, Object> result : results) {
	
			Question question = new Question();
			Answer answer = new Answer();
			Survey survey = new Survey();
	
			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);
	
			question.setId((Long)result.get("id"));
			question.setQuetext((String)result.get("quetext"));
			question.setLanguage((String)result.get("language"));
			question.setVisibility(true);
	
			answer.setId((Long)result.get("id"));
			answer.setAnstext((String)result.get("anstext"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);
	
			surveys.add(survey);
		}
	
		return surveys;
	
	}

	@Override
	public List<Survey> getAllSurveyEn() {
	
	
		List<Survey> surveys = new ArrayList<Survey>();
	
		String sql = String.format("SELECT * FROM Surveys s WHERE s.visibility = true AND s.language='EN'"); 
	
		List<Map<String, Object>> results = select(sql);
	
		for(Map<String, Object> result : results) {
	
			Question question = new Question();
			Answer answer = new Answer();
			Survey survey = new Survey();
	
			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);
	
			question.setId((Long)result.get("id"));
			question.setQuetext((String)result.get("quetext"));
			question.setLanguage((String)result.get("language"));
			question.setVisibility(true);
	
			answer.setId((Long)result.get("id"));
			answer.setAnstext((String)result.get("anstext"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);
	
			surveys.add(survey);
		}
	
		return surveys;
	
	}

	@Override
	public List<Survey> searchSurvey(String column, String value) {
	
		List<Survey> entitiesList = new ArrayList<Survey>();
	
		String sql = "SELECT * FROM Surveys as s WHERE s.visibility = true AND " + column + " = '" + value+"'";
	
		List<Map<String, Object>> results = select(sql);
	
		for(Map<String, Object> result : results) {
	
			Survey survey = new Survey();
	
	
			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);
	
			entitiesList.add(survey);
		}
		return entitiesList;
	}

	@Override
	public void addSurvey(Survey survey) {
	
		String sql = String.format("INSERT INTO Surveys (typology, language, visibility) VALUES ('%s', '%s', true)",
				survey.getTypology(), survey.getLanguage());
	
		req(sql);
	}

	@Override
	public void updateSurvey(Survey survey) {
	
		String sql = String.format("UPDATE Surveys s "
				+ "SET typology = '%s', language = '%s', visibility = true WHERE s.id = %d", 
				survey.getId(), survey.getTypology(), survey.getLanguage());
	
		req(sql);
	}

	@Override
	public void deleteSurvey(Survey survey) {
	
		/*String sql = String.format("SELECT * FROM Answers a, Questions q WHERE a.idquestion= q.id AND q.id=%d", survey.getQuestions().get(0).getId());
		System.out.println("1"+sql);
		System.out.println(survey.getQuestions().get(0).getId());
		System.out.println(select(sql).size());
		if(select(sql).size() > 0) {
	
		sql = String.format("SELECT * FROM Questions q, Surveys s WHERE q.idsurvey = s.id AND s.id = %d", survey.getId());
	
		System.out.println("2"+sql);
		System.out.println(survey.getId());
		System.out.println(select(sql).size());
	
		if(select(sql).size() > 0) {
	
		sql = String.format("UPDATE Surveys s, Questions q, Answers a "
				+ "SET s.visibility = false, q.visibility = false, a.visibility = false "
				+ "WHERE s.id = q.idsurvey AND q.id = a.idquestion AND s.id = %d", survey.getId());
	
		System.out.println("3"+sql);
	
	} else {*/
		String sql = String.format("UPDATE Surveys s SET s.visibility = false WHERE s.id = %d", survey.getId());
	
	
		//}
		req(sql);
	}

	@Override
	public void addQuestion(Question question, Long idSurvey) {

		String sql = String.format("INSERT INTO Questions (quetext, idsurvey, language, visibility) "
				+ "VALUES ('%s', %d, '%s', true)", 
				question.getQuetext(), idSurvey, question.getLanguage());

		req(sql);
	}

	public void addQuestions(List<Question> questions, Long idSurvey) {
		for(Question question : questions) {
			String sql = String.format("INSERT INTO Questions (quetext, idsurvey, language, visibility) "
					+ "VALUES ('%s', %d, '%s', true)", 
					question.getQuetext(), idSurvey, question.getLanguage());

			req(sql);
		}
	}

	public List<Question> getQuestions (Long id) {

		List<Question> questions = new ArrayList<Question>();

		String sql = String.format("SELECT * FROM Questions q INNER JOIN Surveys s ON s.id = q.idsurvey WHERE s.id =" 
				+ id + " AND q.visibility = true"); 

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {
			Survey survey = new Survey();
			Question question = new Question();
			Answer answer = new Answer();

			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);

			answer.setId((Long)result.get("id"));
			answer.setAnstext((String)result.get("anstext"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);

			question.setId((Long)result.get("id"));
			question.setQuetext((String)result.get("quetext"));
			question.setLanguage((String)result.get("language"));
			question.setVisibility(true);

			questions.add(question);
		}
		return questions;
	}

	@Override
	public void add(Question entity) {


	}

	@Override
	public void update(Question entity) {


	}

	@Override
	public void delete(Question question) {

		String sql = String.format("UPDATE Questions q SET q.visibility = false WHERE q.id = %d", question.getId());

		req(sql);
	}

	@Override
	public List<Question> findAll() {


		List<Question> questions = new ArrayList<Question>();

		String sql = String.format("SELECT * FROM Questions q WHERE q.visibility = true"); 

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Question question = new Question();
			Answer answer = new Answer();
			Survey survey = new Survey();

			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);

			question.setId((Long)result.get("id"));
			question.setQuetext((String)result.get("quetext"));
			question.setLanguage((String)result.get("language"));
			question.setVisibility(true);

			answer.setId((Long)result.get("id"));
			answer.setAnstext((String)result.get("anstext"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);

			questions.add(question);
		}
		return questions;
	}

	
	@Override
	public List<Question> search(String column, String value) {

		return null;
	}
	
	@Override
	public void addAnswer(Answer answer, Long idQuestion) {

		String sql = String.format("INSERT INTO Answers (anstext, value, idquestion, language, visibility) "
				+ "VALUES ('%s', %d, %d, '%s', true)", 
				answer.getAnstext(), answer.getValue(), idQuestion, answer.getLanguage());

		req(sql);
	}
	
	@Override
	public void addAnswers(List<Answer> answers, Long idQuestion) {
		for(Answer answer : answers) {
			String sql = String.format("INSERT INTO Answers (anstext, value, idquestion, language, visibility) "  
				+ "VALUES ('%s', %d, %d, '%s', true)", 
					answer.getAnstext(), answer.getValue(), idQuestion, answer.getLanguage());

			req(sql);
		}
	}

	@Override
	public List<Answer> getAllAnswer() {


		List<Answer>  answers = new ArrayList<Answer>();
		List<Question>  questions = new ArrayList<Question>();

		String sql = String.format("SELECT * FROM Answers a WHERE a.visibility = true"); 

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Question question = new Question();
			Answer answer = new Answer();
			Survey survey = new Survey();
			
			answer.setId((Long)result.get("id"));
			answer.setAnstext((String)result.get("anstext"));
			answer.setValue((Long)result.get("value"));
			answer.setLanguage((String)result.get("language"));
			answer.setVisibility(true);
			
			question.setAnswers(answers);
			question.setId((Long)result.get("id"));
			question.setQuetext((String)result.get("quetext"));
			question.setLanguage((String)result.get("language"));
			question.setVisibility(true);
			
			survey.setQuestions(questions);
			survey.setId((Long)result.get("id"));
			survey.setTypology((String)result.get("typology"));
			survey.setLanguage((String)result.get("language"));
			survey.setVisibility(true);

			answers.add(answer);
		}
		return answers;
	}

	@Override
	public void deleteAnswer(Answer answer) {
	
		String sql = String.format("UPDATE Answers a SET a.visibility = false WHERE a.id = %d", answer.getId());
	
		req(sql);
	}

	public List<Question> getWithoutAnswer(Long idSurvey) {
		
		String sql = "SELECT * FROM Questions q where q.idsurvey= "+ idSurvey + " AND q.id NOT IN "
				+ "( SELECT q.id  FROM Questions q INNER JOIN Answers a ON q.id = a.idquestion  "
				+ "INNER JOIN Surveys s  ON s.id = q.idsurvey   WHERE s.visibility = true "
				+ "AND q.visibility = true AND  s.id ="+ idSurvey +")" ;
		
		List<Map<String, Object>> results = select(sql);
	
		Map<Long, Question> questions = new LinkedHashMap<>();
	
		
		for(Map<String, Object> result : results) {

			Question question = new Question();

				question.setId((Long)result.get("id"));
				question.setQuetext((String)result.get("quetext"));
				question.setLanguage((String)result.get("language"));
				question.setVisibility(true);
	
				questions.put((Long)result.get("id"), question);
		}
		return new ArrayList<Question>(questions.values()); 
	}

	
	
	//////////////////////////////////////////
	
	Date data = new Date();
	
	@Override
	public void addEmail(Email email) {

		String sql = String.format("INSERT INTO Emails (emailtext, data, visibility) VALUES ('%s', current_date(), true)", 
				email.getEmailtext());
		req(sql);
	}
	
	@Override
	public Collection<Email> getAllEmail() {
		
		List<Email> emails = new ArrayList<Email>();

		String sql = String.format("SELECT * FROM Emails s WHERE s.visibility = true"); 

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Email email = new Email();
			
			email.setId((Long)result.get("id"));
			email.setEmailtext((String)result.get("emailtext"));
			email.setData(new Date(((Timestamp)result.get("data")).getTime()));
			email.setVisibility(true);
	
			emails.add(email);
		}
		return emails;
	}
	
	////////////////////////////////////

	@Override
	public void addTip(Tip tip) {

		String sql = String.format("INSERT INTO Tips (tiptext, data, visibility) VALUES ('%s', current_date(), true)", 
				tip.getTiptext());
		req(sql);
	}

	
	@Override
	public Collection<Tip> getAllTip() {
		
		List<Tip> tips = new ArrayList<Tip>();

		String sql = String.format("SELECT * FROM Tips s WHERE s.visibility = true"); 

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Tip tip = new Tip();
			
			tip.setId((Long)result.get("id"));
			tip.setTiptext((String)result.get("tiptext"));
			tip.setData(new Date(((Timestamp)result.get("data")).getTime()));
			tip.setVisibility(true);
	
			tips.add(tip);
		}
		return tips;
	}

	@Override
	public void addChoice (Choice choice) {
	
		String sql = String.format("INSERT INTO Choices (idquestion, idanswer, count, visibility) VALUES "
				+ "(%d, %d, %d, true)", choice.getIdQuestion(), choice.getIdAnswer(), choice.getCount());
	
		req(sql);
	}

	@Override
	public List<Question> getAllStatistic(Long idSurvey) {
		
		String sql = "SELECT c.idQuestion, q.quetext, c.idAnswer, a.anstext, COUNT(c.id) As count FROM Choices As c, Questions AS q, Answers AS a, Surveys AS s "
				+ "WHERE c.idQuestion= q.id AND c.idAnswer= a.id AND s.id= q.idSurvey AND s.id =" + idSurvey + " GROUP BY c.idAnswer, c.idQuestion ";

		List<Map<String, Object>> results = select(sql);
		

		Map<Long, Question> questions = new LinkedHashMap<>();

		for(Map<String, Object> result : results) {

				Question question = new Question();				
				
				Answer answer = new Answer();

				answer.setId((Long)result.get("idAnswer"));
			
				answer.setAnstext((String)result.get("anstext"));
				answer.setValue((Long)result.get("value"));
				answer.setLanguage((String)result.get("language"));
				answer.setVisibility(true);	

				
				question.setId((Long)result.get("idQuestion"));
				question.setQuetext((String)result.get("quetext"));
				question.setLanguage((String)result.get("language"));
				question.setVisibility(true);
				
				if (questions.containsKey(question.getId())) {
					questions.get(question.getId()).getAnswers().add(answer);
					
				} else {
					List<Answer> answers = new ArrayList<>();
					answers.add(answer);
					
					question.setAnswers(answers);
					questions.put(question.getId(), question);
				}
			}

		return new ArrayList<Question>(questions.values()); 
	}

	
	@Override
	public Question findById(Long id) {
	
		return null;
	}
}