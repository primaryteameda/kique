package it.primaryteam.webapp.kique.survey.tips;

import java.util.Date;
import it.primaryteam.webapp.kique.core.BaseDomain;


public class Tip extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;
	
	private String tiptext;
	private Date data ;
	
	public String getTiptext() {
		return tiptext;
	}
	public void setTiptext(String tiptext) {
		this.tiptext = tiptext;
	}

	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "Tip [tiptext=" + tiptext + ", data=" + data + "]";
	}
}
