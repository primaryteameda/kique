package it.primaryteam.webapp.kique.survey.question;

import java.util.ArrayList;
import java.util.List;
import it.primaryteam.webapp.kique.core.BaseDomain;
import it.primaryteam.webapp.kique.survey.answer.Answer;

public class Question extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;

	private String quetext;
	private String language;
	List<Answer> answers = new ArrayList <Answer>();

	public String getQuetext() {
		return quetext;
	}
	public void setQuetext(String quetext) {
		this.quetext = quetext;
	}
	public List<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	@Override
	public String toString() {
		return "Question [quetext=" + quetext + ", language=" + language + ", answers=" + answers + "]";
	}

}