package it.primaryteam.webapp.kique.login;

import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import it.primaryteam.webapp.kique.core.BaseRepository;
import it.primaryteam.webapp.kique.login.Login;


@Dependent
public class LoginRepository extends BaseRepository <Long, Login>{

	public boolean getLogin(Login login)  {

		Login entity = new Login();

		List<Map<String, Object>> results;

		String sql = "SELECT *  FROM Login as l WHERE l.visibility = true AND l.username ='" + login.getUsername() 
		+ "' AND l.password ='"  + login.getPassword() + "'";

		results = select(sql);

		if(results.size()>0) {

			for(Map<String, Object> result : results) {

				entity.setId((Long)result.get("id"));
				login.setUsername((String)result.get("username"));
				login.setPassword((String)result.get("password"));
				login.setLevel((String)result.get("level"));
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public void add(Login entity) {
	}

	@Override
	public Login findById(Long id) {
		return null;
	}

	@Override
	public void update(Login entity) {
	}

	@Override
	public void delete(Login entity) {
	}

	@Override
	public List<Login> findAll() {
		return null;
	}

	@Override
	public List<Login> search(String column, String value) {
		return null;
	}

}