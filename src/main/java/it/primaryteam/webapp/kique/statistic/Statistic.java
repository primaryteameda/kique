package it.primaryteam.webapp.kique.statistic;

import java.util.ArrayList;
import java.util.List;

import it.primaryteam.webapp.kique.core.BaseDomain;
import it.primaryteam.webapp.kique.survey.question.Question;

public class Statistic extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;
	
	private List<Question> cho = new ArrayList<Question>();

	public List<Question> getCho() {
		return cho;
	}

	public void setCho(List<Question> cho) {
		this.cho = cho;
	}

	
		
}
