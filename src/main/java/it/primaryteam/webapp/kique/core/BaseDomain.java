package it.primaryteam.webapp.kique.core;

import java.io.Serializable;

public abstract class BaseDomain<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;

	protected ID id;
	private boolean visibility;

	public boolean isVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		return this.id.equals(((BaseDomain<?>)obj).getId());
	}
}
