package it.primaryteam.webapp.kique.core;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseRepository<ID extends Serializable,  D extends BaseDomain<ID>> implements Repository <ID, D>{

	private static final String URL="jdbc:mysql://192.168.30.111:3306/kique?serverTimezone=UTC&autoReconnect=true&useSSL=false";
	//private static final String URL="jdbc:mysql://localhost:3306/kique?serverTimezone=UTC&autoReconnect=true&useSSL=false";

	private static final String USER = "root";
	private static final String PASSWORD = "test";

	public void req(String sql) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD))  {
			Statement statement = connection.createStatement();
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
	}

	public List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnCount = rsmd.getColumnCount();

			while(resultSet.next()) {
				Map<String, Object> result = new HashMap<>();

				for (int i = 1; i <= columnCount; i++) {
					String columnName = rsmd.getColumnLabel(i);
					result.put(columnName, resultSet.getObject(columnName));
				}
				results.add(result);
			}
		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
		return results;
	}

}