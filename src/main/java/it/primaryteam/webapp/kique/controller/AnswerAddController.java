package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.survey.answer.Answer;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class AnswerAddController  extends BaseController{

	private static final long serialVersionUID = 1L;

	private Answer answer;

	@Inject
	private QuestionRepo answerRepository;

	private List<Answer> answers;
	private List<Answer> selectedAnswers = new ArrayList<>();

	private List<Answer> answersToSend = new ArrayList<>();
	private List<Answer> selectedAnswersToSend = new ArrayList<>();
	
	private Long idQuestion;

	@Override
	public void init() {
		answer= new Answer();
		answers = answerRepository.getAllAnswer();
	}

	public void addAnswer() {
		idQuestion = Long.valueOf(parameters.get("idQuestion"));
		answerRepository.addAnswers(answersToSend, idQuestion);
		redirect("surveyAdmin.xhtml");
	}

	public void addNewAnswer() {
		Long idQuestion = Long.valueOf(parameters.get("idQuestion"));
		answerRepository.addAnswer(answer, idQuestion);
	}

	public void addSelectedAnswer(Answer answer) {
		if (answer != null && !selectedAnswers.contains(answer)) {
			selectedAnswers.add(answer);
		}
	}

	public void removeSelectedAnswer(Answer answer) {
		selectedAnswers.remove(answer);
	}

	public void addAnswerToSend(Answer answer) {
		if (!selectedAnswersToSend.contains(answer)) {
			selectedAnswersToSend.add(answer);
		}
	}

	public void removeAnswerToSend(Answer answer) {
		selectedAnswersToSend.remove(answer);
	}

	public void populateAnswersToSend() {
		for(Answer selectedAnswer : selectedAnswers) {
			if (!answersToSend.contains(selectedAnswer)) {
				answersToSend.add(selectedAnswer);
				answers.remove(selectedAnswer);
			}
		}

		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addAnswerAdminForm:answersList");		
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addAnswerAdminForm:selectedAnswersList");	
	}

	public void removeAnswersToSend() {
		answersToSend.removeAll(selectedAnswersToSend);

		for (Answer selectedAnswerToSend : selectedAnswersToSend) {
			if (!answers.contains(selectedAnswerToSend)) {
				answers.add(selectedAnswerToSend);
			}
		}

		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addAnswerAdminForm:answersList");		
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addAnswerAdminForm:selectedAnswersList");		
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public List<Answer> getSelectedAnswers() {
		return selectedAnswers;
	}

	public void setSelectedAnswers(List<Answer> selectedAnswers) {
		this.selectedAnswers = selectedAnswers;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public List<Answer> getAnswersToSend() {
		return answersToSend;
	}

	public void setAnswersToSend(List<Answer> answersToSend) {
		this.answersToSend = answersToSend;
	}
}
