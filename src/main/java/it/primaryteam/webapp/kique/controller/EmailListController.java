package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.email.Email;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;


@ManagedBean
@ViewScoped
public class EmailListController extends BaseController{

	private static final long serialVersionUID = 1L;
	
	private Email email;
	
	Collection<Email> emails = new ArrayList<Email>();

	@Inject
	private QuestionRepo emailRepository;
	
	@Override
	public void init() {
		emails = emailRepository.getAllEmail();
		
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Collection<Email> getEmails() {
		return emails;
	}

	public void setEmails(Collection<Email> emails) {
		this.emails = emails;
	}
}