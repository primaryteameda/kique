package it.primaryteam.webapp.kique.controller;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.survey.Survey;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;


@ManagedBean
@ViewScoped
public class SurveyListControllerIt extends BaseController{

	private static final long serialVersionUID = 1L;
	
	private Survey survey;
	private List<Question> questions;
	private List<Survey> surveys;

	

	@Inject
	private QuestionRepo questionRepository;
	
	@Inject
	private QuestionRepo surveyRepository;
	
	@Override
	public void init() {
		surveys = surveyRepository.getAllSurveyIt();
		
	}

	public void deleteSurvey(Survey survey) {

		questionRepository.deleteSurvey(survey);
		FacesContext.getCurrentInstance().addMessage("Messaggio Video", new FacesMessage("Questionario Cancellato"));
		surveys = surveyRepository.getAllSurvey();
	}
	
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public List<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(List<Survey> surveys) {
		this.surveys = surveys;
	}
	
	
}
