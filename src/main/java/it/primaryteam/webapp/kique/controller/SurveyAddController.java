package it.primaryteam.webapp.kique.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.survey.Survey;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;


@ManagedBean
@ViewScoped
public class SurveyAddController  extends BaseController{

	private static final long serialVersionUID = 1L;
	
	private Survey survey;
	
	@Inject
	private QuestionRepo questionRepository;
	
	@Override
	public void init() {
		survey = new Survey();
	}
	
	public void addSurvey() {
	
		questionRepository.addSurvey(survey);
		redirect("surveyAdmin.xhtml");
	}
	
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}
}
