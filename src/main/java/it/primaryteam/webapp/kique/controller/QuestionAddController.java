package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class QuestionAddController  extends BaseController{

	private static final long serialVersionUID = 1L;

	private Question question;

	@Inject
	private QuestionRepo questionRepository;

	private List<Question> questions;
	private List<Question> selectedQuestions = new ArrayList<>();

	private List<Question> questionsToSend = new ArrayList<>();
	private List<Question> selectedQuestionsToSend = new ArrayList<>();

	@Override
	public void init() {
		question= new Question();
		questions = questionRepository.findAll();
	}

	public void addQuestion() {
		Long idSurvey = Long.valueOf(parameters.get("idSurvey"));
		questionRepository.addQuestions(questionsToSend, idSurvey);
		redirect("questionWithoutAnswer.xhtml?idSurvey=" + idSurvey);
	}
	
	public void addNewQuestion() {
		Long idSurvey = Long.valueOf(parameters.get("idSurvey"));
		questionRepository.addQuestion(question, idSurvey);
		redirect("questionWithoutAnswer.xhtml?idSurvey=" + idSurvey);
	}


	public void addSelectedQuestion(Question question) {
		if (question != null && !selectedQuestions.contains(question)) {
			selectedQuestions.add(question);
		}
	}

	public void removeSelectedQuestion(Question question) {
		selectedQuestions.remove(question);
		//FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:questionsList");
		// FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:selectedQuestionsList");		
	}

	public void addQuestionToSend(Question question) {
		if (!selectedQuestionsToSend.contains(question)) {
			selectedQuestionsToSend.add(question);
		}
	}

	public void removeQuestionToSend(Question question) {
		selectedQuestionsToSend.remove(question);
		//FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:questionsList");
		// FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:selectedQuestionsList");		
	}

	public void populateQuestionsToSend() {
		for(Question selectedQuestion : selectedQuestions) {
			if (!questionsToSend.contains(selectedQuestion)) {
				questionsToSend.add(selectedQuestion);
				questions.remove(selectedQuestion);
			}
		}

		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:questionsList");		
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:selectedQuestionsList");	
	}

	public void removeQuestionsToSend() {
		questionsToSend.removeAll(selectedQuestionsToSend);

		for (Question selectedQuestionToSend : selectedQuestionsToSend) {
			if (!questions.contains(selectedQuestionToSend)) {
				questions.add(selectedQuestionToSend);
			}
		}

		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:questionsList");		
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("addQuestionForm:selectedQuestionsList");		
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public List<Question> getSelectedQuestions() {
		return selectedQuestions;
	}

	public void setSelectedQuestions(List<Question> selectedQuestions) {
		this.selectedQuestions = selectedQuestions;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public List<Question> getQuestionsToSend() {
		return questionsToSend;
	}

	public void setQuestionsToSend(List<Question> questionsToSend) {
		this.questionsToSend = questionsToSend;
	}
}
