package it.primaryteam.webapp.kique.controller;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.controller.BaseController;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class QuestionWithoutAnswerController extends BaseController {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private QuestionRepo questionRepository;
	
	private List<Question> questions;
	
	private Long idSurvey;

	@Override
	public void init() {
		idSurvey = Long.valueOf(parameters.get("idSurvey"));
		questions = questionRepository.getWithoutAnswer(idSurvey);
	}
	
	public void deleteQuestion(Question question) {
		questionRepository.delete(question);
		questions= questionRepository.getWithoutAnswer(idSurvey);
	}
	
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public String getId(Question question) {
		return "" + System.currentTimeMillis();
	}

	public Long getIdSurvey() {
		return idSurvey;
	}

	public void setIdSurvey(Long idSurvey) {
		this.idSurvey = idSurvey;
	}
}
