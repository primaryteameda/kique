package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.controller.BaseController;
import it.primaryteam.webapp.kique.survey.choice.Choice;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class QuestionListController extends BaseController {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private QuestionRepo questionRepository;
	
	private List<Question> questions;
	
	private boolean disabled;
	
	private Map<Long, Long> choices = new HashMap<>();
	
	private List<String> answers = new ArrayList<>();
	
	private Choice choice;
	
	private Long idSurvey;

	private Map<String, String> selectedAnswers = new HashMap<>();
	

	@Override
	public void init() {
		choice= new Choice();
		idSurvey = Long.valueOf(parameters.get("idSurvey"));
		questions = questionRepository.getAllBySurvey(idSurvey);
		disabled = false;
	}	
	
	public void submit() {
		System.out.println("test");
	}
	
	public void addSurvey() {
		System.out.println("");
	}
	
	public void deleteQuestion(Question question) {
		questionRepository.delete(question);
		questions = questionRepository.getAllBySurvey(idSurvey);
	}

	public boolean enableToSend() {
		return choices.size() == questions.size();
	}
	
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Map<Long, Long> getChoices() {
		return choices;
	}

	public void setChoices(Map<Long, Long> choices) {
		this.choices = choices;
	}
	
	public String getId(Question question) {
		return "" + System.currentTimeMillis();
	}
	
	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public Map<String, String> getSelectedAnswers() {
		return selectedAnswers;
	}

	public void setSelectedAnswers2(Map<String, String> selectedAnswers) {
		this.selectedAnswers = selectedAnswers;
	}

}
