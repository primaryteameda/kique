package it.primaryteam.webapp.kique.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;
import it.primaryteam.webapp.kique.survey.tips.Tip;

@ManagedBean
@ViewScoped
public class TipPagesController extends BaseController{
	
	
	private static final long serialVersionUID = 1L;
	
	private Tip tip;
	
	@Inject
	private QuestionRepo tipRepository;
	
	
	@Override
	public void init() {
		tip= new Tip();
	}
	
	public void addTip() {
		tipRepository.addTip(tip); 
		redirect("endSurvey.xhtml");
	}

	public Tip getTip() {
		return tip;
	}

	public void setTip(Tip tip) {
		this.tip = tip;
	}

}
