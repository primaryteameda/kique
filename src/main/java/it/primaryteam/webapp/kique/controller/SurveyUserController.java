package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.primaryteam.webapp.kique.survey.answer.Answer;
import it.primaryteam.webapp.kique.survey.choice.Choice;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class SurveyUserController extends BaseController {


	@Inject
	private QuestionRepo questionRepository;

	private static final long serialVersionUID = 1L;
	private Long idSurvey;

	List<String> answers = new ArrayList<>();

	private List<Question> questions;

	private Answer answer;
	private boolean disabled;

	private Map<Long, Long> selectedAnswers = new HashMap<>();

	private Map<Long, Long> selectedChoice;

	int cont = 0;

	@Override
	public void init() {
		answer = new Answer();
		idSurvey = Long.valueOf(parameters.get("idSurvey"));
		questions = questionRepository.getAllBySurvey(idSurvey);

		for (int i = 1; i < 6; i++) {
			answers.add("" + i);
		}
	}

	public void addFinal() {
		
		for(Entry<Long, Long> selectedChoice : selectedAnswers.entrySet()) {
			Choice choice = new Choice();
			choice.setIdQuestion(selectedChoice.getKey());
			choice.setIdAnswer(Long.valueOf(String.valueOf(selectedChoice.getValue())));
			questionRepository.addChoice(choice);
		}
	redirect("tip.xhtml");
	}



	public boolean enableToSend() {

		return !(selectedAnswers.size() == questions.size());
	}

	public void test() {
		System.out.println("test");
	}


	public void submit() {
		System.out.println("test");
	}


	public List<String> getAnswers() {
		return answers;
	}

	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}

	public Map<Long, Long> getSelectedAnswers() {
		return selectedAnswers;
	}

	public void setSelectedAnswers(Map<Long, Long> selectedAnswers) {
		this.selectedAnswers = selectedAnswers;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public Long getIdSurvey() {
		return idSurvey;
	}

	public void setIdSurvey(Long idSurvey) {
		this.idSurvey = idSurvey;
	}

}
