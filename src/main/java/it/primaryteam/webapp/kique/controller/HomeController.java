package it.primaryteam.webapp.kique.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class HomeController extends BaseController {

	private static final long serialVersionUID = 1L;

	@Override
	public void init() {
	}
}
