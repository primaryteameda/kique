package it.primaryteam.webapp.kique.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.primaryteam.webapp.kique.email.Email;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class PagesEmailController extends BaseController{
	
	
	private static final long serialVersionUID = 1L;
	
	private Email email;
	
	@Inject
	private QuestionRepo emailRepository;
	
	
	@Override
	public void init() {
		email= new Email();
	}
	
	public void addEmail() {
		emailRepository.addEmail(email); 
		redirect("home.xhtml");
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

}
