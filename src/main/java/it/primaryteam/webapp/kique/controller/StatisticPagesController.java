package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import it.primaryteam.webapp.kique.statistic.Statistic;
import it.primaryteam.webapp.kique.survey.answer.Answer;
import it.primaryteam.webapp.kique.survey.question.Question;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class StatisticPagesController extends BaseController{

	private static final long serialVersionUID = 1L;
	
	private Statistic statistic;
	private List<Question> questions;

	
	@Inject
	private QuestionRepo statisticRepository;
	
	private  Answer answer ;
	
	@Override
	public void init() {
		answer= new Answer();
		statistic = new Statistic();
		Long idSurvey = Long.valueOf(parameters.get("idSurvey"));
		questions = statisticRepository.getAllStatistic(idSurvey);
	}

	public Statistic getStatistic() {
		return statistic;
	}

	public void setStatistic(Statistic statistic) {
		this.statistic = statistic;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}
	
}
