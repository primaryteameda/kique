package it.primaryteam.webapp.kique.controller;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.login.Login;
import it.primaryteam.webapp.kique.login.LoginRepository;

@ManagedBean		
@RequestScoped  
public class LoginController extends BaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Login login;

	@Inject  
	private LoginRepository loginRepository;

	public void init() {
		login= new Login();
		loginRepository = new LoginRepository();
	}


	public void checkLogin() {

		Boolean authenticated = loginRepository.getLogin(login);

		if (authenticated) {
			redirect("homeAdmin.xhtml");

		} else {
				FacesContext.getCurrentInstance().addMessage("Errore", new FacesMessage("Utente o Password Errata"));
		}
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}
}
