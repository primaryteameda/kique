package it.primaryteam.webapp.kique.controller;

import java.util.ArrayList;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;
import it.primaryteam.webapp.kique.survey.tips.Tip;


@ManagedBean
@ViewScoped
public class TipListController extends BaseController{

	private static final long serialVersionUID = 1L;
	
	private Tip tip;
	
	Collection<Tip> tips = new ArrayList<Tip>();

	@Inject
	private QuestionRepo tipRepository;
	
	@Override
	public void init() {
		tips = tipRepository.getAllTip();
		
	}

	public Tip getTip() {
		return tip;
	}

	public void setTip(Tip tip) {
		this.tip = tip;
	}

	public Collection<Tip> getTips() {
		return tips;
	}

	public void setTips(Collection<Tip> tips) {
		this.tips = tips;
	}
}