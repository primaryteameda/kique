package it.primaryteam.webapp.kique.controller;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.kique.controller.BaseController;
import it.primaryteam.webapp.kique.survey.answer.Answer;
import it.primaryteam.webapp.kique.survey.repository.QuestionRepo;

@ManagedBean
@ViewScoped
public class AnswerListController extends BaseController {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private QuestionRepo answerRepository;
	
	private List<Answer> answers;
	
	private Long idQuestion;

	@Override
	public void init() {
		idQuestion = Long.valueOf(parameters.get("idQuestion"));
		answers = answerRepository.getAllAnswer();
	}
	
	public void deleteAnswer(Answer answer) {
		answerRepository.deleteAnswer(answer);
		answers = answerRepository.getAllAnswer();
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
	
}
