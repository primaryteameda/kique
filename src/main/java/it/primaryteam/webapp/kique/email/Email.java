package it.primaryteam.webapp.kique.email;

import java.util.Date;

import it.primaryteam.webapp.kique.core.BaseDomain;

public class Email extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;
	
	private String emailtext;
	private Date data ;

	public String getEmailtext() {
		return emailtext;
	}

	public void setEmailtext(String emailtext) {
		this.emailtext = emailtext;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Email [emailtext=" + emailtext + ", data=" + data + "]";
	}
	
}
